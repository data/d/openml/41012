# OpenML dataset: NPSdecay

https://www.openml.org/d/41012

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Andrea Coraddu, Francesca Cipollini, Luca Oneto, Davide Anguita  
**Please cite**: Cipollini, F. and Oneto, L. and Coraddu, Murphy, A.J. and Anguita, D., Condition-Based Maintenance of Naval Propulsion Systems: Data Analysis with Minimal Feedback, RESS (Under review), 2018.  

**Dataset**:
Hull, Propeller and Gas Turbine efficiency decay: Data Analysis with Minimal Feedback

**Abstract**:
The proposed dataset is the result over experiments carried out by means of a numerical simulator of a naval vessel (Frigate), characterized by a COmbined Diesel ELectric And Gas (CODLAG) propulsion plant.
The different blocks forming the complete simulator have been developed adopting Matlab and Simulink frameworks, and fine tuned over the years by means of real operational data of similar propulsion plants.These blocks describe the behaviour of the main components of a Naval Propulsion System &#40;NPS&#41;, which are the gas turbine (GT), the gas turbine compressor (GTC), the hull (HLL), and the propeller (PRP).
In view of these observations, the available data are in agreement with a possible real vessel.
In this release of the simulator it is also possible to take into account the performance decay over time of the PRP Thrust, PRP Torque, the HLL, the GTC and GT, according to several vessel parameters.
A series of measures (25 features) which indirectly represents of the state of the system subject to performance decay has been acquired and stored in the dataset over the parameter's space.

For each record it is provided:

* - A 25-feature vector containing the vessel relevant features:
*  Lever (lp) [ ]; 
*  Vessel Speed [knots]; 
*  Gas Turbine shaft torque (GTT) [kN m]; 
*  Gas Turbine Speed (GT rpm) [rpm]; 
*  Controllable Pitch Propeller Thrust stbd (CPP T stbd)[N]; 
*  Controllable Pitch Propeller Thrust port (CPP T port)[N]; 
*  Shaft Torque port (Q port) [kN]; 
*  Shaft rpm port (rpm port)[rpm]; 
*  Shaft Torque stbd (Q stdb) [kN]; 
*  Shaft rpm stbd (rpm stbd) [rpm]; 
*  HP Turbine exit temperature (T48) [C]; 
*  Generator of Gas speed (GG rpm) [rpm]; 
*  Fuel flow (mf) [kg/s];  
*  ABB Tic control signal (ABB Tic) []; 
*  GT Compressor outlet air pressure (P2) [bar];  
*  GT Compressor outlet air temperature (T2) [C];  
*  External Pressure (Pext) [bar];  
*  HP Turbine exit pressure (P48) [bar];  
*  TCS tic control signal (TCS tic) [];  
*  Thrust coefficient stbd (Kt stbd) [];  
*  Propeller rps stbd (rps prop stbd) [rps];  
*  Thrust coefficient port (Kt port) [];  
*  Propeller rps port (rps prop port) [rps];  
*  Propeller Torque port (Q prop port) [Nm];  
*  Propeller Torque stbd (Q prop stbd) [Nm];       
* - Propeller Thrust decay state coefficient (Kkt); 
* - Propeller Torque decay state coefficient (Kkq); 
* - Hull decay state coefficient (Khull); 
* - GT Compressor decay state coefficient (KMcompr); 
* - GT Turbine decay state coefficient (KMturb).

**Notes**: 
- Features are not normalized
- Each feature vector is a row on the text file &#40;30 elements in each row&#41;

For more information about this dataset please contact: cbm@smartlab.ws
Check at www.cbm.smartlab.ws for updates on this dataset.

**License**:
Use of this dataset in publications must be acknowledged by referencing the following publication

This dataset is distributed AS-IS and no responsibility implied or explicit can be addressed to the authors or their institutions for its use or misuse. Any commercial use is prohibited.

**Other Related Publications**:
* [1] Cipollini, F., Oneto, L., Coraddu, A., Murphy, A. J., & Anguita, D. (2018). Condition-Based Maintenance of Naval Propulsion Systems with supervised Data Analysis. Ocean Engineering, 149, 268-278.
* [2] Coraddu, A. and Oneto, L. and Ghio, A. and Savio, S. and Anguita, D. and Figari, M. - Journal: Proceedings of the Institution of Mechanical Engineers Part M: Journal of Engineering for the Maritime Environment - Number: 1 - Pages: 136-153 - Title: Machine learning approaches for improving condition-based maintenance of naval propulsion plants - Volume: 230 - Year: 2016.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41012) of an [OpenML dataset](https://www.openml.org/d/41012). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41012/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41012/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41012/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

